/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [`./index.html`, `./src/**/*.{vue,js,ts,jsx,tsx}`],
  theme: {
    extend: {
      colors: {
        'black-base': `#1F1F1F`,
        'red-base': `#E50914`,
        'red-secondary': `#FF0742`,
        'gray-base': `#D2D2D2`,
        'gray-secondary': `#929292`,
        'grey-ternary': '#b1b1b1',
        'green-base': `#0ACF83`,
        'orange-base': `#F7941E`,
        'blue-base': `#1C9CD2`
      },
      boxShadow: {
        profile: `0px 0px 4px rgba(5, 5, 5, 0.08);`,
        header: `0px 4px 4px rgba(0, 0, 0, 0.25);`
      },
      fontFamily: {
        sans: [`Fira Sans Condensed`, ...defaultTheme.fontFamily.sans]
      }
    },
    screens: {
      sm: { min: '640px', max: '767px' },
      // => @media (min-width: 640px and max-width: 767px) { ... }

      md: { min: '768px', max: '1023px' },
      // => @media (min-width: 768px and max-width: 1023px) { ... }

      lg: { min: '1024px', max: '1279px' },
      // => @media (min-width: 1024px and max-width: 1279px) { ... }

      xl: { min: '1280px', max: '1535px' },
      // => @media (min-width: 1280px and max-width: 1535px) { ... }

      '2xl': { min: '1536px' }
      // => @media (min-width: 1536px) { ... }
    }
  },
  plugins: []
}
