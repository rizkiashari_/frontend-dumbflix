export interface IModal {
  isOpen: boolean
  setIsOpen: (isOpen: boolean) => void
}
