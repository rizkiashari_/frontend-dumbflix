/* eslint-disable multiline-ternary */
import React, { useState } from 'react'
import { Link, useLocation } from 'react-router-dom'
import Logo from '../../../assets/images/logo.png'
import { MenuIcon, XIcon } from '@heroicons/react/solid'
import RegisterModal from './RegisterModal'
import LoginModal from './LoginModal'

const Header = () => {
  const currentRoute = useLocation().pathname

  const [isOpen, setIsOpen] = useState(false)
  const [isOpenRegist, setIsOpenRegist] = useState(false)
  const [isOpenLogin, setIsOpenLogin] = useState(false)

  return (
    <nav className='py-4 shadow-header bg-black-base px-4 sticky w-full top-0'>
      <div className='containers flex justify-between items-center'>
        <ul
          className={`2xl:flex md:flex xl:flex lg:flex 2xl:flex-row md:flex-row xl:flex-row lg:flex-row flex-col absolute 2xl:items-center xl:items-center lg:items-center md:items-center justify-between 2xl:static xl:static lg:static md:static w-full 2xl:w-fit xl:w-fit lg:w-fit md:w-fit ${
            isOpen
              ? 'top-16 left-0  bg-black-base p-4 duration-100'
              : '-top-72 transition-all p-4 bg-black-base duration-200 left-0'
          }`}
        >
          <li className='px-4 py-2'>
            <Link
              to='/'
              className={` ${
                currentRoute === '/'
                  ? 'border-b-slate-300 border-b-2'
                  : 'hover:border-b-2 hover:border-b-slate-300'
              } pb-2`}
            >
              Home
            </Link>
          </li>
          <li className='px-4 py-2 2xl:mx-2'>
            <Link
              to='/tv-shows'
              className={` ${
                currentRoute === '/tv-shows'
                  ? 'border-b-slate-300 border-b-2'
                  : 'hover:border-b-2 hover:border-b-slate-300'
              } pb-2`}
            >
              Tv Shows
            </Link>
          </li>
          <li className='px-4 py-2'>
            <Link
              to='/movies'
              className={` ${
                currentRoute === '/movies'
                  ? 'border-b-slate-300 border-b-2'
                  : 'hover:border-b-2 hover:border-b-slate-300'
              } pb-2`}
            >
              Movies
            </Link>
          </li>
          <li className='px-4 py-2 2xl:hidden xl:hidden lg:hidden md:hidden mt-4'>
            <Link
              to='/auth/register'
              className='bg-white text-red-base px-6 py-2 font-bold rounded-md'
            >
              Register
            </Link>
          </li>
          <li className='px-4 py-2 2xl:hidden xl:hidden lg:hidden md:hidden mt-3'>
            <Link
              to='/auth/login'
              className='bg-red-base text-white px-6 py-2 font-bold rounded-md'
            >
              Login
            </Link>
          </li>
        </ul>
        <div className='2xl:w-32 xl:w-32 lg:w-32 md:w-32 w-24 mt-2'>
          <Link to='/'>
            <img src={Logo} className='object-cover' />
          </Link>
        </div>
        <div className='hidden 2xl:flex xl:flex lg:flex md:flex'>
          <button
            onClick={() => setIsOpenRegist(true)}
            className='bg-white text-red-base px-6 py-2 font-bold rounded-md mr-5'
          >
            Register
          </button>
          <button
            onClick={() => setIsOpenLogin(!isOpenLogin)}
            className='bg-red-base text-white px-6 py-2 font-bold rounded-md'
          >
            Login
          </button>
        </div>
        {isOpenRegist && (
          <RegisterModal isOpen={isOpenRegist} setIsOpen={setIsOpenRegist} />
        )}
        {isOpenLogin && (
          <LoginModal isOpen={isOpenLogin} setIsOpen={setIsOpenLogin} />
        )}
        {isOpen ? (
          <XIcon
            width={32}
            onClick={() => setIsOpen(false)}
            className='absolute cursor-pointer right-4 top-5 2xl:hidden xl:hidden lg:hidden md:hidden'
          />
        ) : (
          <MenuIcon
            width={32}
            onClick={() => setIsOpen(true)}
            className='absolute cursor-pointer right-4 top-5 2xl:hidden xl:hidden lg:hidden md:hidden'
          />
        )}
      </div>
    </nav>
  )
}

export default Header
