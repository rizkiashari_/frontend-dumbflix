/* eslint-disable multiline-ternary */
import React, { Fragment, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { XCircleIcon } from '@heroicons/react/solid'
import { IModal } from '../../interface/Modal'
import FormLogin from '../atoms/FormLogin'
import FormRegister from '../atoms/FormRegister'

const LoginModal: React.FC<IModal> = ({ isOpen, setIsOpen }) => {
  const [isSwitchModal, setIsSwitchModal] = useState(false)

  const closeModal = () => {
    setIsOpen(false)
  }

  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog as='div' className='relative z-10' onClose={closeModal}>
        <Transition.Child
          as={Fragment}
          enter='ease-out duration-300'
          enterFrom='opacity-0'
          enterTo='opacity-100'
          leave='ease-in duration-200'
          leaveFrom='opacity-100'
          leaveTo='opacity-0'
        >
          <div className='fixed inset-0 bg-black bg-opacity-50' />
        </Transition.Child>

        <div className='fixed inset-0 overflow-y-auto'>
          <div className='flex min-h-full items-center justify-center p-4 text-center'>
            <Transition.Child
              as={Fragment}
              enter='ease-out duration-300'
              enterFrom='opacity-0 scale-95'
              enterTo='opacity-100 scale-100'
              leave='ease-in duration-200'
              leaveFrom='opacity-100 scale-100'
              leaveTo='opacity-0 scale-95'
            >
              <Dialog.Panel className='w-full max-w-md transform overflow-hidden rounded-2xl bg-black-base px-8 py-6 text-left align-middle shadow-xl transition-all'>
                <div className='flex items-center justify-between'>
                  <Dialog.Title
                    as='h3'
                    className='text-2xl font-medium leading-6 text-white'
                  >
                    {!isSwitchModal ? 'Login' : 'Register'}
                  </Dialog.Title>
                  <XCircleIcon
                    className='w-10 cursor-pointer'
                    onClick={closeModal}
                  />
                </div>
                <div className='mt-6'>
                  {!isSwitchModal ? <FormLogin /> : <FormRegister />}
                  <p className='text-gray-secondary text-center'>
                    {!isSwitchModal
                      ? 'Dont have an account ?'
                      : 'Already have an account ?'}{' '}
                    <b
                      onClick={() => setIsSwitchModal(!isSwitchModal)}
                      className='cursor-pointer'
                    >
                      Click Here
                    </b>
                  </p>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  )
}

export default LoginModal
