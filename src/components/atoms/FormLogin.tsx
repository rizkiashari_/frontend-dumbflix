/* eslint-disable multiline-ternary */
import React, { useState } from 'react'
import { EyeIcon, EyeOffIcon } from '@heroicons/react/solid'

const FormLogin: React.FC = () => {
  const [isShowPass, setIsShowPass] = useState(false)

  return (
    <form className='mt-4'>
      <input
        className='w-full px-4 py-2 rounded-md mb-5 border-2 border-gray-secondary bg-gray-base bg-opacity-25 placeholder:text-grey-ternary outline-none'
        placeholder='Email'
        type='email'
      />
      <div className='relative mb-4'>
        <input
          className='w-full px-4 py-2 rounded-md mb-5 border-2 border-gray-secondary bg-gray-base bg-opacity-25 placeholder:text-grey-ternary outline-none'
          placeholder='Password'
          type={`${isShowPass ? 'text' : 'password'}`}
        />
        {isShowPass ? (
          <EyeIcon
            onClick={() => setIsShowPass(!isShowPass)}
            className='w-6 h-6 absolute top-3 right-4 cursor-pointer'
          />
        ) : (
          <EyeOffIcon
            onClick={() => setIsShowPass(!isShowPass)}
            className='w-6 h-6 absolute top-3 right-4 cursor-pointer'
          />
        )}
      </div>
      <button className='bg-red-base text-white py-2 text-base font-semibold w-full rounded-lg mb-3'>
        Login
      </button>
    </form>
  )
}

export default FormLogin
