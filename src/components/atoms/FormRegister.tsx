/* eslint-disable multiline-ternary */
import { ChevronDownIcon, EyeIcon, EyeOffIcon } from '@heroicons/react/solid'
import React, { useState } from 'react'

const FormRegister: React.FC = () => {
  const [isShowPass, setIsShowPass] = useState(false)

  return (
    <form className='mt-6'>
      <input
        className='w-full px-4 py-2 rounded-md mb-5 border-2 border-gray-secondary bg-gray-base bg-opacity-25 placeholder:text-grey-ternary outline-none'
        placeholder='Email'
        type='email'
      />
      <div className='relative'>
        <input
          className='w-full px-4 py-2 rounded-md mb-5 border-2 border-gray-secondary bg-gray-base bg-opacity-25 placeholder:text-grey-ternary outline-none'
          placeholder='Password'
          type={`${isShowPass ? 'text' : 'password'}`}
        />
        {isShowPass ? (
          <EyeIcon
            onClick={() => setIsShowPass(!isShowPass)}
            className='w-6 h-6 absolute top-3 right-4 cursor-pointer'
          />
        ) : (
          <EyeOffIcon
            onClick={() => setIsShowPass(!isShowPass)}
            className='w-6 h-6 absolute top-3 right-4 cursor-pointer'
          />
        )}
      </div>
      <input
        className='w-full px-4 py-2 rounded-md mb-5 border-2 border-gray-secondary bg-gray-base bg-opacity-25 placeholder:text-grey-ternary outline-none'
        placeholder='Full Name'
        type='text'
      />
      <div className='relative mb-5'>
        <select
          className='form-select appearance-none block w-full px-4 py-2 text-base bg-clip-padding bg-no-repeat border-solid border-gray-secondary bg-gray-base bg-opacity-25 placeholder:text-grey-ternary rounded transition ease-in-out m-0 text-grey-ternary border-2'
          aria-label='Gender'
        >
          <option hidden>Gender</option>
          <option value='female'>Female</option>
          <option value='male'>Male</option>
        </select>
        <ChevronDownIcon className='w-7 text-gray-base absolute top-2 right-4' />
      </div>
      <input
        className='w-full px-4 py-2 rounded-md mb-5 border-2 border-gray-secondary bg-gray-base bg-opacity-25 placeholder:text-grey-ternary outline-none'
        placeholder='Phone'
        type='text'
      />
      <textarea
        className='w-full px-4 py-2 rounded-md mb-5 border-2 border-gray-secondary bg-gray-base bg-opacity-25 placeholder:text-grey-ternary resize-none outline-none'
        placeholder='Address'
        rows={3}
      ></textarea>
      <button className='bg-white text-red-base py-2 text-base font-semibold w-full rounded-lg mb-3'>
        Register
      </button>
    </form>
  )
}

export default FormRegister
