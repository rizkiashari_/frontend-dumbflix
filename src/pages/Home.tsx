import React from 'react'
import Layout from './Layout'
import Banner from '../../assets/images/dummy-banner.png'

const Home = () => {
  return (
    <Layout>
      <div className='banner -z-30'>
        <img
          src={Banner}
          className='w-full h-full object-cover -z-20'
          alt='banner'
        />
        <div className='absolute  2xl:top-56 xl:top-56 lg:top-56 md:top-56 top-16 2xl:left-32 xl:left-32 lg:left-32 md:left-32 left-8 z-50 text-white'>
          <h3 className='2xl:text-9xl xl:text-7xl lg:text-7xl md:text-7xl sm:text-5xl text-4xl'>
            THE WITCHER
          </h3>
          <p className='xl:text-2xl 2xl:text-2xl md:text-lg text-base 2xl:max-w-2xl xl:max-w-2xl lg:max-w-2xl md:max-w-2xl max-w-lg mt-4 pr-8'>
            Geralt of Rivia, a solitary monster hunter, struggles to find his
            place in a world where people often prove more wicked than beast
          </p>
          <div className='flex gap-6 items-center mt-2'>
            <p>2019</p>
            <p className='border-2 px-3 py-1 rounded shadow-md'>Tv Series</p>
          </div>
          <button className='mt-6 2xl:text-4xl xl:text-4xl lg:text-4xl md:text-4xl text-xl bg-red-base px-12 rounded-md py-4'>
            Watch Now!
          </button>
        </div>
      </div>
    </Layout>
  )
}

export default Home
