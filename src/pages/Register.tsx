import React from 'react'
import FormRegister from '../components/atoms/FormRegister'
import Layout from './Layout'

const Register = () => {
  return (
    <Layout>
      <div className='flex justify-center items-center relative -z-30'>
        <div className='px-4 containers'>
          <h3 className='mt-8 text-2xl font-bold'>Register</h3>
          <FormRegister />
        </div>
      </div>
    </Layout>
  )
}

export default Register
