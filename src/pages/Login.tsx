import React from 'react'
import FormLogin from '../components/atoms/FormLogin'
import Layout from './Layout'

const Login: React.FC = () => {
  return (
    <Layout>
      <div className='flex justify-center items-center relative -z-30'>
        <div className='px-4 containers'>
          <h3 className='mt-8 text-2xl font-bold'>Login</h3>
          <FormLogin />
        </div>
      </div>
    </Layout>
  )
}

export default Login
