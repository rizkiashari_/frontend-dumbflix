import React from 'react'
import Header from '../components/molekul/Header'

interface ILayout {
  children: React.ReactNode
}

const Layout: React.FC<ILayout> = ({ children }) => {
  return (
    <>
      <Header />
      <div className='font-sans'>{children}</div>
    </>
  )
}

export default Layout
